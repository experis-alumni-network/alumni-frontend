# Alumni network portal
---
### Group members

The partners who collaborated on the frontend was Sinan Karagülle, Henrik Eijsink and Martin Johansen.

---
### Task description

This project was created to help previous students and asscociates of both Noroff and Experis to keep in touch. Our primary task was to create this solution with the goal in mind of making it easier to connect the user to social events

---
### Project technical details

To run the project you first need to clone it to your local computer.

When this is done you have no run ' *npm install* ' in your command line, this could take 1-2 minutes.
 
While it is downloading you should install the Angular CLI tool. For this you need to open CMD for Windows and terminal for Mac and type 'npm install -g @angular/cli'. 

After everything is installed you can open the project in your favorite IDE and type 'ng serve -o' into the command line. After some loading the project should open in your browser.

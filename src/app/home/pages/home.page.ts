import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from 'src/app/post/models/post';
import { User } from 'src/app/profile/models/user';
import { PostService } from 'src/app/shared/services/post.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.css']
})
export class HomePage implements OnInit {

  public posts : Post[] = [];
  public users : User[] = [];
  

  constructor(private readonly postService : PostService,
    private readonly sessionService : SessionService, 
    private readonly router : Router, private readonly userService : UserService) { }

  async ngOnInit(): Promise<void> {
    if(Object.keys(this.sessionService.currentUser).length === 0) {
      this.router.navigate(['/login'])
    }
    this.posts = await this.postService.getPosts();
    const postAuthorsId = new Set(this.posts.map(post => post.author));

    this.users = await this.userService.getListOfUsers(postAuthorsId);
    //test
  }
}

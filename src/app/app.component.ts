import { Component } from '@angular/core';
import { TimeagoIntl } from 'ngx-timeago';
import { strings as englishStrings} from 'ngx-timeago/language-strings/en'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'alumni-frontend';
  constructor(intl: TimeagoIntl) {
    intl.strings = englishStrings;
    intl.changes.next()
  }
}

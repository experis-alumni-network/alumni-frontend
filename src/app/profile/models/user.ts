import { Post } from "src/app/post/models/post";
import { Topic } from "src/app/topic/models/topic";

export interface User {
authoredComments: Comment[],
authoredPosts: Post[],
bio: string,
email: string,
funFact: string,
imgURL: string,
name: string,
occupation: string,
provider: string,
subscribedPosts: Post[],
topics: Topic[],
userId: number
}
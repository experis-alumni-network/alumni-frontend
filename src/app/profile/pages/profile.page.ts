import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/profile/models/user';
import { SessionService } from 'src/app/shared/services/session.service';
// import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})
export class ProfilePage implements OnInit {
  loggedIn: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sessionService: SessionService,
  ) { }

  ngOnInit(): void {
    //this.sessionService.fetchUser() //development only, not nessecary, login should do this.

    this.loggedIn = this.sessionService.currentUser != undefined
    if(this.loggedIn && this.router.url.endsWith("profile"))
      this.router.navigate([this.sessionService.currentUser?.userId], { relativeTo: this.route })
  }

  goToLogin(): void{
    this.router.navigate(["login"]);
  }
}


import { Component, OnInit, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/profile/models/user';
import { UserService } from 'src/app/shared/services/user.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { PostService } from 'src/app/shared/services/post.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  authorized: boolean = false;
  private id: number = 0;
  user: User | undefined;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private postService : PostService,
    private userService:UserService,
    private sessionService: SessionService,
  ) { }
  
  async ngOnInit(): Promise<void>{
    this.id = Number(this.route.snapshot.paramMap.get("id"))
    this.user = await this.userService.getUser(this.id);
    if(this.id === this.sessionService.currentUser?.userId){ 
      this.user = this.sessionService.currentUser
      this.authorized = true
    }
    else{//only get user if not yourself
      this.authorized = false
    }
  }

  editProfile(): void{
    this.router.navigate(["edit"], { relativeTo: this.route });
  }

  goToDashboard(): void{
    this.router.navigate(["dashboard"], { relativeTo: this.route });
  }
}

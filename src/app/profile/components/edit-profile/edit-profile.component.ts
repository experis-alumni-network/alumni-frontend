import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/profile/models/user';
import { SessionService } from 'src/app/shared/services/session.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-edit-profile-component',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
})
export class EditProfileComponent implements OnInit,OnDestroy {
  @Input() user = {} as User;
  id: number = 0;
  updatedUrl: string = "";
  updatedStatus: string = "";
  updatedBio: string = "";
  updatedFunFact: string = "";
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sessionService: SessionService,
    private userService:UserService
  ) { }
  async ngOnDestroy(): Promise<void> {
    await this.userService.putUser(this.user)
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get("id"))
    if(this.id != this.sessionService.currentUser.userId){
      this.router.navigate(['../'], { relativeTo: this.route })
      alert("ID theft alert! You cannot edit somebody elses profile!")
    }
    else{
      this.user = this.sessionService.currentUser
      this.updatedUrl = this.user!.imgURL
    }
  }

  async ngOnSubmit(postForm: NgForm): Promise<void> {
    this.user!.imgURL = this.updatedUrl
    this.user!.occupation = postForm.value.updatedStatus
    this.user!.bio = postForm.value.updatedBio
    this.user!.funFact = postForm.value.updatedFunFact
    await this.userService.putUser(this.user).then(async () => {
      this.userService.openSnackBarMessage("You updated your profile","Wicked!")
      await this.router.navigate(["../"], { relativeTo: this.route });
    })
  }
}

import { Injectable } from '@angular/core';
import { Post } from 'src/app/post/models/post';
import { environment } from 'src/environments/environment';
import { Topic } from '../models/topic';

@Injectable({
  providedIn: 'root'
})
export class TopicService {
  public topics : Topic[] = [];
  public error : string = "";
  private _authToken : string = "";

  constructor() {
    this._authToken = localStorage.getItem("authToken")!;
   }

  async getTopics() : Promise<Topic[]> {
      const response = await fetch(`${environment.baseURL}topic`, {
        headers : {
          'Content-Type' : 'application/json',
          'authorization' : `Bearer ${this._authToken}`
        }
      });
      return await response.json();
  }

  async getTopic(id : number) : Promise<Topic> {
    const response = await fetch(`${environment.baseURL}topic/${id}`, {
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : `Bearer ${this._authToken}`
      }
    });
    return await response.json();
  }

  async joinTopic(topicid : number) : Promise<void> {
    const response = await fetch(`${environment.baseURL}topic/subscribe/${topicid}`,{
      method : 'PUT',
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : `Bearer ${this._authToken}`
      }
    });
    await response.json();
  }

  async getTopicPosts(topicId : number) : Promise<Post[]> {
    return await fetch(`${environment.baseURL}topic/${topicId}/post`, {
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : `Bearer ${this._authToken}`
      }
    }).then(response => {
      return response.json();
    }).catch(error => {
      console.log(error);
      return [];
    })
  }

  async postTopic(topic : Topic) : Promise<void> {
    const response = fetch(`${environment.baseURL}topic`, {
      method : 'POST',
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : `Bearer ${this._authToken}`
      },
      body : JSON.stringify(topic),
    }).catch(error => {
      console.log(error);
    })
  }
}

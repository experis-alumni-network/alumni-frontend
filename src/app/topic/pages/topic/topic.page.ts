import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.page.html',
  styleUrls: ['./topic.page.css']
})
export class TopicPage implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
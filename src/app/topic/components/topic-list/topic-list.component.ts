import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/shared/services/session.service';
import { Topic } from '../../models/topic';
import { TopicService } from '../../services/topic.service';

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.css']
})
export class TopicListComponent implements OnInit {
  public subscribed : boolean = false;
  public topics : Topic[] = [];
  constructor(private readonly topicService : TopicService, 
  private readonly sessionService : SessionService,
  private readonly router : Router) { }

  async ngOnInit(): Promise<void> {
    this.topics = await this.topicService.getTopics();
  }
  async updateTopic() {
    await this.topicService.getTopics();
  }

  goToCreateTopic() {
    this.router.navigate(['/topic/create']);
  }
}

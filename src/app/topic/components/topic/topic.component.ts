import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/shared/services/post.service';
import { Topic } from '../../models/topic';
import { TopicService } from '../../services/topic.service';
import { DashboardComponent } from 'src/app/shared/components/dashboard/dashboard.component';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {
  private id : number = 0;

  public topic : Topic = {} as Topic;

  constructor(
    private readonly route : ActivatedRoute,
     private readonly postService : PostService,
     private readonly topicService : TopicService) { }

  async ngOnInit(): Promise<void> {
    this.route.params.subscribe(params => {this.id = +params.id})
    this.topicService.getTopic(this.id).then(t => this.topic = t);
  }
}

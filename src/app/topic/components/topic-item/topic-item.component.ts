import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/shared/services/session.service';
import { UserService } from 'src/app/shared/services/user.service';
import { Topic } from '../../models/topic';
import { TopicService } from '../../services/topic.service';

@Component({
  selector: 'app-topic-item',
  templateUrl: './topic-item.component.html',
  styleUrls: ['./topic-item.component.css']
})
export class TopicItemComponent implements OnInit {
  @Input() topic : Topic = {} as Topic;
  @Output() update : EventEmitter<string> = new EventEmitter();
  public isSubscribed : boolean = false;
  currentUserId : number = 0;


  

  constructor(private readonly topicService : TopicService, 
    private readonly userService : UserService,
    private readonly router : Router,
    private readonly sessionService : SessionService) { }

  ngOnInit(): void {
    this.currentUserId = this.sessionService.currentUser.userId;
    this.topic.interestedUsers.forEach(id => {
      if(id === this.currentUserId) {
        this.isSubscribed = true;
      }
    })
  }

  async subscribeToTopic(topicId : number) {
    await this.topicService.joinTopic(topicId);
    this.userService.openSnackBarMessage("Subscribed", "Awesome!");
    this.isSubscribed = true;
  }

  goToTopic(id : number) {
    this.router.navigate([`topic/${id}`]);
  }
}

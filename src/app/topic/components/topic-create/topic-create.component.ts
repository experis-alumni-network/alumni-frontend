import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.service';
import { Topic } from '../../models/topic';
import { TopicService } from '../../services/topic.service';

@Component({
  selector: 'app-topic-create',
  templateUrl: './topic-create.component.html',
  styleUrls: ['./topic-create.component.css']
})
export class TopicCreateComponent implements OnInit {

  constructor(private readonly topicService : TopicService,
    private userService : UserService, private readonly router : Router) { }

  ngOnInit(): void {
  }

  onSubmit(form : NgForm) {
    console.log(form.value);
    
    const topic : Topic = {} as Topic;
    topic.name = form.value.topicName;
    topic.description = form.value.topicDesc;
    this.topicService.postTopic(topic).then(() => {
      this.userService.openSnackBarMessage("Topic created", "Easy Money");
      this.router.navigate(['/topic'])
    });
  }

  onBackClick(){
    this.router.navigate(["topic"])
  }
}

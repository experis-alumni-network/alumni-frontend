export interface Topic {
    topicId : number,
    name : string,
    description : string,
    interestedUsers : number[],
    posts : number[],
};
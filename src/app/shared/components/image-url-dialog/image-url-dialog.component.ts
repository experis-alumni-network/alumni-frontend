import { Component, OnInit } from '@angular/core';
import { inject } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-image-url-dialog',
  templateUrl: './image-url-dialog.component.html',
  styleUrls: ['./image-url-dialog.component.css']
})
export class ImageUrlDialogComponent implements OnInit {
  public url : string = "";

  constructor(
    public dialogRef: MatDialogRef<ImageUrlDialogComponent>,
  ) { }

  ngOnInit() : void {
  }
  onSubmit() : void{
    this.dialogRef.close(this.url);
  }

}

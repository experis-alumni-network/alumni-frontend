import { ChangeDetectionStrategy, Component, Input, OnInit, SimpleChange } from '@angular/core';
import { filter } from 'rxjs/operators';
import { Post } from 'src/app/post/models/post';
import { User } from 'src/app/profile/models/user';
import { PostService } from '../../services/post.service';
import { Event } from 'src/app/post/models/event';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeedComponent implements OnInit {
  @Input() posts : Post[] = [];
  @Input() users : User[] = [];
  public filteredPosts : Post[] = [];
  public normalFeed : boolean = true;
  public searchText : string = "";
  public currentFilter : string = "";
  constructor(
    private readonly postService : PostService,
  ) { }
  ngOnInit(): void {
  }

  ngOnChanges(changes : SimpleChange) {
    if(this.posts){
      this.posts = this.posts.sort((a, b) =>new Date(b.lastUpdated).getTime() - new Date(a.lastUpdated).getTime());
    }
    
  }
  handleFilterClick(value : string) {
    this.filteredPosts = [];
    this.posts.forEach(post => {
      //Removes the added markdown on the values
      //E.g **strong** = strong
      let title = post.title.replace(/[^a-zA-Z]/g,"").toLowerCase().includes(value.toLowerCase())
      let content = post.content.replace(/[^a-zA-Z]/g,"").toLowerCase().includes(value.toLowerCase());

      //If either the tilte or body contains a match, add the post to an array
      if(title || content) {
        this.filteredPosts.push(post);
      }
    })
    this.normalFeed = false;
    this.currentFilter = value;
  }

  getUserForItem(userId : number){
    return this.users.find(user => user.userId == userId)!
  }
  resetFilters() {
    this.normalFeed = true;
    this.filteredPosts = [];
    this.searchText = "";
  }

}

import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostEvent } from 'src/app/post/models/postEvent';
import { PostService } from '../../services/post.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-calendar-event',
  templateUrl: './calendar-event.component.html',
  styleUrls: ['./calendar-event.component.css'],
})
export class CalendarEventComponent implements OnInit {
  @Input() event: PostEvent = {} as PostEvent;
  public username : string = "";

  constructor(
    private readonly userService : UserService,
    private router: Router,
  ) { }

  async ngOnInit(): Promise<void> {
    this.event.startTime = new Date(this.event.startTime);
    this.event.endTime = new Date(this.event.endTime)
    this.username = (await this.userService.getUser(this.event.author)).name;
  }

  getTime(date:Date): string {
    return String(date.getHours()).padStart(2,'0') + ":" + String(date.getMinutes()).padStart(2,'0')
  }

  clickUser(id:number): void {
    this.router.navigate(['profile/', id])
  }

  clickEvent(id:number): void {
    this.router.navigate(['post/', id])
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { User } from 'src/app/profile/models/user';
import { SessionService } from '../../services/session.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private readonly userService : UserService,
    private authService: SocialAuthService,
    private sessionService:SessionService,
    private router : Router) { }

  async ngOnInit(): Promise<void> {
  }
  get loggedIn(){
    return this.sessionService.isLoggedIn;
  }
  onLogOut(){
    this.authService.signOut(true);
    localStorage.clear();
    this.sessionService.isLoggedIn = false;
    this.sessionService.authToken = "";
    this.router.navigate(['login']);
    this.userService.openSnackBarMessage("You are logged out!","Got it!");
  }
}

import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EVENTS } from 'src/app/mock-events';
import { CalendarService } from '../../services/calendar.service';
import { SessionService } from '../../services/session.service';
import { NgIf } from '@angular/common';
import { UserService } from '../../services/user.service';
import { Post } from 'src/app/post/models/post';
import { PostEvent } from 'src/app/post/models/postEvent';
import { PostService } from '../../services/post.service';
// import { POSTS } from 'src/app/mock-posts';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent implements OnInit{
  @Input() posts: Post[] = []
  public events : PostEvent[] = [];
  private id: number = 0
  today: Date | undefined
  nextMonth: Date[] | undefined
  document: Document = new Document()
  days: string[] = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  months: string[] = ['January','February','March','April','May','June','July','August','September','October','November','December'];


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private calendarService: CalendarService,
    private readonly postService : PostService,
    private sessionService: SessionService,
  ) { }

  async ngOnInit(): Promise<void> { 
    this.id = Number(this.route.snapshot.paramMap.get("id"))
    await this.convertPosts().then(() => {
      if(this.events.length > 0) {
        this.today = new Date()
        this.initNextMonth()
      }
    });
  }

  async convertPosts() : Promise<void> {
    return await new Promise(resolve => {
      this.posts.forEach(async (post : Post) => {
        this.events.push(await this.postService.getEvent(post.postId));
        resolve();
      });
    })
  } 

  initNextMonth(): void{
    this.nextMonth = [];
    for(let step = 0; step < 30; step++){
      let day = new Date().getDate() + step;
      let month = new Date().getMonth();
      let year = new Date().getFullYear();
      this.nextMonth.push(new Date(year,month,day,0,0,0,0))
    }
  }

  //This is a horrible solution, should be refractored
  //I made it even worse, Big pog
  checkDay(day : Date, event : Date ): boolean{
    event = new Date(event);
    if(day.getDate() == event.getDate() && day.getMonth() == event.getMonth() && day.getFullYear() == event.getFullYear()) {
      return true;
    } else {
      return false;
    }
  }
}



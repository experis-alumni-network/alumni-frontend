import { Component, Input, OnInit } from '@angular/core';
import { Post } from 'src/app/post/models/post';
import { Event } from 'src/app/post/models/event';
import { PostService } from '../../services/post.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupService } from 'src/app/group/services/group.service';
import { TopicService } from 'src/app/topic/services/topic.service';
import { PostEvent } from 'src/app/post/models/postEvent';
import { User } from 'src/app/profile/models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public feedPosts : Post[] = [];
  public users : User[] = [];
  public calenderEvents : Post[] = [];
  public viewFeed : boolean = false;
  public profile : boolean = false;
  public group : boolean = false;
  public topic : boolean = false;
  private sharedId : number = 0;
  public isLoaded : boolean = false;

  constructor(private readonly postService : PostService, 
    private readonly topicService : TopicService,
    private readonly userService : UserService,
  private readonly router : Router, 
  private readonly route : ActivatedRoute,
  private readonly groupService : GroupService) { }

  async ngOnInit(): Promise<void> {
    let string = this.router.url.split('/')[1];
    this.route.params.subscribe(param => {
      this.sharedId = param.id;
    })
    if(string === "profile") {
      this.splitEventsFromPosts(await this.postService.getPostsPostedByUser(this.sharedId));
      console.log(this.feedPosts);
      
    } else if(string === "group") {
      this.splitEventsFromPosts(await this.groupService.getGroupPosts(this.sharedId));
    } else if(string === "topic") {
      this.splitEventsFromPosts(await this.topicService.getTopicPosts(this.sharedId));
    }
    
    // this.feedPosts = this.feedPosts.sort((a, b) => new Date(b.lastUpdated).getTime() - new Date(a.lastUpdated).getTime())
    // const postAuthorsIds = new Set(this.feedPosts.map(post => post.author));
    // this.users = await this.userService.getListOfUsers(postAuthorsIds);
    
    // console.log(this.feedPosts);
    // console.log(this.users);

  }

  async splitEventsFromPosts(array : Post[]) {
    
    let idList = new Set(array.map(post => post.author));
    array.forEach(async (post : Post) => {
      if(post.type === 1) {
        this.calenderEvents.push(post);
      } else {
        this.feedPosts.push(post);
      }
    })
    this.users = await this.userService.getListOfUsers(idList);
    this.isLoaded = true;
  }

  //Change the current view
  handleClickView() {
    this.viewFeed = !this.viewFeed;
  }

}

import { Component, ElementRef, EventEmitter, Input, OnInit, Output, SystemJsNgModuleLoader, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { COMMA, ENTER} from '@angular/cdk/keycodes';
import { map, startWith } from 'rxjs/operators';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocomplete, MatAutocompleteActivatedEvent, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { UserService } from '../../services/user.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-invite-dialog',
  templateUrl: './invite-dialog.component.html',
  styleUrls: ['./invite-dialog.component.css']
})
export class InviteDialogComponent{
  @Input() allNames: string[] = ["Henrik", "Martin", "Tien", "Sinan"]
  selectable = true;
  removable = true;
  names: string[] = [];
  filteredNames: Observable<string[]>;
  nameControl = new FormControl();
  separatorKeysCodes: number[] = [ENTER, COMMA];
  autocompleteTagsOptionActivated = false;
  @Output() submitClicked = new EventEmitter<any>();

  //TODO: Use User-objects instead of names, to handle users with the same name
  
  @ViewChild('nameInput') nameInput: ElementRef<HTMLInputElement> | undefined;
  
  constructor(
    private userService : UserService,
    public dialogRef: MatDialogRef<InviteDialogComponent>
  ) {
    this.filteredNames = this.nameControl.valueChanges.pipe(
      startWith(null),
      map((name: string | null) => name ? this._filter(name) : this.allNames!.slice()))  
  }

  optionActivated(event: MatAutocompleteActivatedEvent) {
    if (event.option) {
      this.autocompleteTagsOptionActivated = true;
    }
  }
  addWithEnter(event : MatChipInputEvent): void{
    if (this.autocompleteTagsOptionActivated){
      return
    }
    const value = event.value
    event.chipInput!.clear();
    this.autocompleteTagsOptionActivated = false
    this.add(value)
  }
  
  selected(event: MatAutocompleteSelectedEvent): void{
    this.add(event.option.viewValue)
  }
  
  add(inputName : string): void{
    if(!this.allNames.includes(inputName)){
      this.userService.openSnackBarMessage("There is no user named \"" + inputName + "\"", "okey :/")
    }
    else if(this.names.includes(inputName)){
      this.userService.openSnackBarMessage("You have already added \"" + inputName + "\"", "okey :/")
    }
    else{
      this.names.push(inputName);
      this.nameInput!.nativeElement.value = '';
      this.nameControl.setValue(null);
    }
  }
  

  onSubmit() : void{
    this.dialogRef.close(this.names);
    this.userService.openSnackBarMessage("Invite sent!", "Nice");
  }

  remove(name: string): void{
    const index = this.names.indexOf(name);
  
    if (index >= 0){
      this.names.splice(index, 1);
    }
  }
  
  private _filter(value: string): string[]{
    const filterValue = value.toLowerCase();
    const filtered = this.allNames!.filter(name => name.toLowerCase().includes(filterValue)).filter(name => !this.names.includes(name));
    return filtered
  }
}

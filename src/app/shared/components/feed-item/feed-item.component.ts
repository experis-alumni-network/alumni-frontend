import { ChangeDetectionStrategy, Component, Input, OnInit, SimpleChange } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from 'src/app/post/models/post';
import snarkdown from 'snarkdown';
import { UserService } from '../../services/user.service';
import { SessionService } from '../../services/session.service';
import { User } from 'src/app/profile/models/user';
import { Group } from 'src/app/group/models/group';
import { PostService } from '../../services/post.service';
import { group } from '@angular/animations';
import { GroupService } from 'src/app/group/services/group.service';
import { Event } from 'src/app/post/models/event';
import { LoginPage } from 'src/app/login/login.page';

@Component({
  selector: 'app-feed-item',
  templateUrl: './feed-item.component.html',
  styleUrls: ['./feed-item.component.css']
})
export class FeedItemComponent implements OnInit {
  @Input() post : Post = {} as Post;
  // @Input() users:User[] = [];
  public author : string = "";
  public content : string = "";
  public title : string = "";
  public date : string = ""
  @Input() user : User = {} as User;
  public group : Group | undefined;
  public live = true;


  constructor(
    private readonly router : Router, 
    private readonly userService : UserService,
    private readonly sessionService : SessionService,
    private readonly postService : PostService,
    private readonly groupService : GroupService,) { }

  async ngOnInit(): Promise<void> {
    
    if(this.post?.author === undefined && this.user !== undefined) {
      this.post.author = this.user.userId;
      this.user = this.sessionService.currentUser;
    }
    if(this.post!.type == 1 && this.post.event == undefined){
      this.post!.event = await this.postService.getShortEvent(this.post!.postId)
    }

    if(this.post!.group != 0 && this.post!.group != undefined){
      this.group = await this.groupService.getGroup(this.post!.group!)
    }
  }

  
  ngOnChanges(changes : SimpleChange) {
    this.handleSnarkdownOfInput(this.post);
    
  }

  //Redirects user to a spesific post
  handleClickOnPost(id : number) {
      this.router.navigate([`/post/${id}`]);
  }

  handleClickOnUser(id : number) {
    this.router.navigate([`/profile/${id}`])
  }
  handleClickOnGroup(id : number) {
    this.router.navigate([`/group/${id}`])
  }

  //Converts the markdwon into the innerHTML
  handleSnarkdownOfInput(post? : Post) {
    this.title = snarkdown(post!.title)
    this.content = snarkdown(post!.content);
  }
}

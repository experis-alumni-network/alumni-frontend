import { stringify } from '@angular/compiler/src/util';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FileUploadService } from 'src/app/profile/services/file-upload.service';
import { ImageUrlDialogComponent } from '../image-url-dialog/image-url-dialog.component';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  @Input() imgUrl: string = "";
  fileToUpload: File | null = null;
  fileUploadService: FileUploadService | undefined;
  @Output() imgUrlChange: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    public dialog : MatDialog,
    ) {
    this.fileUploadService = new FileUploadService()
  }

  ngOnInit(){}


  openDialog() : void{
    let dialogRef = this.dialog.open(ImageUrlDialogComponent, {
      width: "70%;",
    })
    dialogRef.afterClosed().subscribe(chosenUrl => this.imgUrlChange.emit(chosenUrl));
  }
  
  // handleNewImage(e: Event){
  //   if((e.target as HTMLInputElement).files == null){
  //     //do nothing - maybe clean the file?
  //     return
  //   }
  //   this.fileToUpload = (e.target as HTMLInputElement).files![0];

  //   //TODO:use uploadFileToActivity, and upload the file to the database using the service
  //   this.imgUrlChange.emit("https://via.placeholder.com/250")
  // }
  // uploadFileToActivity(){
  //   this.fileUploadService!.postFile(this.fileToUpload!).subscribe(data => {
  //     //if upload success
  //   }, error => {
  //     //if upload fail
  //     console.log(error)
  //   })
  // }
}


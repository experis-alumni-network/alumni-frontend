import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import { SessionService } from "./session.service";
import { UserService } from "./user.service";

@Injectable({
    providedIn: 'root'
  })
  export class AuthService {
    token:string | null | undefined
    constructor(
        private readonly sessionService : SessionService,
        private readonly userService : UserService) { 
    }
    tokenExpired() {
        this.token = localStorage.getItem('authToken');
        if(!this.token){
            return true;
        }
        const expiry = (JSON.parse(atob(this.token.split('.')[1]))).exp;
        //+300 because token is valid for 5 min after expiry
        return (Math.floor((new Date).getTime() / 1000)) >= expiry + 300;
    }

    async checkIfAuthenticated(){
      
            this.sessionService.authToken = this.token!;
            
            const data = await this.userService.getAuthenticatedUser();
            if(data){
                this.sessionService.currentUser = data;
                this.sessionService.isLoggedIn = true;
            }

        }
}
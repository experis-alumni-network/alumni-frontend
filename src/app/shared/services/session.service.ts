import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { USERS } from '../../mock-users';
import { User } from '../../profile/models/user';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  authToken: string = '';
  isLoggedIn: boolean = false;
  currentUser : User = {} as User;
  
  //fjern
  // _currentUserId : number = 2

  constructor() { }

  // fetchUser() : Observable<User> {
  //   return of(this.currentUser);
  // }

  // setUser(user:User){
  //   this._currentUser = user;
  //   this._currentUserId = user.userId;
  // }

  // get currentUser(){
  //   return this._currentUser;
  // }

  // get currentUserId(){
  //   return this._currentUserId;
  // }
}

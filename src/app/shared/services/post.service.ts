import { HttpClient, HttpHeaders, JsonpClientBackend } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
// import { POSTS } from '../../mock-posts';
import { Post } from '../../post/models/post';
import { Event } from '../../post/models/event'
import { Reply } from '../../post/models/reply';
import { User } from '../../profile/models/user';
import { PostEvent } from 'src/app/post/models/postEvent';
import { Invitation } from 'src/app/post/models/invitation';
import { SessionService } from './session.service';
import { comment } from 'src/app/post/models/comment';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  public _posts : Post[] = [];
  public _userPosts : Post[] = [];
  public _groupPosts : Post[] = [];
  public _topicPosts : Post[] = [];
  private _authToken : string = "";

  constructor(
    private readonly http : HttpClient,
    private readonly sessionService:SessionService) {
    this._authToken = localStorage.getItem("authToken")!;
  }

  async getPosts() : Promise<Post[]> {
    const response = await fetch(`${environment.baseURL}post`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });

    return await response.json();
  }

  async getPost(id : number) : Promise<Post> {
    const response = await fetch(`${environment.baseURL}post/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    return await response.json();
  }

  async getShortEvent(id : number) : Promise<Event> {
    const response = await fetch(`${environment.baseURL}event/short/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    return await response.json();
  }

  async getEvent(id : number) : Promise<PostEvent> {
    const response = await fetch(`${environment.baseURL}event/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    return await response.json();
  }

  //Function that should return posts only meant for a user
  async getPostsPostedByUser(userId : number) : Promise<Post[]> {
    const response = await fetch(`${environment.baseURL}post/getRelevantPosts/user/${userId}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    // console.log(response.json());
    return await response.json();
  }
  async getUserPosts(id : number) : Promise<void> {
    const response = await fetch(`${environment.baseURL}post/user/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    this._userPosts = await response.json();
  }

  //Returns posts for a specific group
  async getGroupPosts(id : number) : Promise<void> {
    const response = await fetch(`${environment.baseURL}post/group/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    this._groupPosts = await response.json();
  }

  //Returns posts for a specific topic
  async getTopicPosts(id : number) : Promise<void> {
    const response = await fetch(`${environment.baseURL}post/topic/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    this._topicPosts = await response.json();
  }

  async getInvitedUsers(id : number) : Promise<User[]> {
    const response = await fetch(`${environment.baseURL}event/${id}/invitedusers`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    return await response.json();
  }

  async getUsersInvitations(id : number) : Promise<Invitation> {
    const response = await fetch(`${environment.baseURL}invitation/user/${id}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    return await response.json();
  }

  async getsingleInvitationForUser(eventId : number, userId : number) : Promise<Invitation> {
    const response = await fetch(`${environment.baseURL}invitation/event/${eventId}/user/${userId}`,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    return await response.json();
  }

  /*  POST CALLS  */

  //Create a new post
  async postPost(post : Post) : Promise<void> {
    await fetch(`${environment.baseURL}post`,{
      method : 'POST',
      body : JSON.stringify(post),
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : 'Bearer ' + this._authToken
      }
    }).then(response => {
      if(!response.ok) {
        throw new Error("Could not create new Post")
      }
    });
  }

  async postEvent(post : Post) : Promise<void> {
    await fetch(`${environment.baseURL}event`, {
      method : 'POST',
      body : JSON.stringify(post),
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : 'Bearer ' + this._authToken
      }
    }).then(response => {
      if(!response.ok) {
        throw new Error("Could not create new Event")
      }
    });
  }

  async postInviteUserToEvent(userId : number, eventId : number) : Promise<void> {
    await fetch(`${environment.baseURL}invitation`, {
      method : 'POST',
      body : JSON.stringify({"event" : eventId, "guest" : userId}),
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : 'Bearer ' + this._authToken
      }
    }).catch(error => {
      console.log(error);
    })
  }


  /* PATCH / PUT CALLS */

  async patchPost(post : Post) : Promise<void> {
    await fetch(`${environment.baseURL}post/update/${post.postId}`, {
      method : 'PATCH',
      body : JSON.stringify(post),
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : 'Bearer ' + this._authToken
      }
    }).then(response => {
      if(!response.ok) {
        throw new Error("Could not edit this Post" + post.postId)
      }
    });
  }

  async patchEvent(post : Post) : Promise<void> {
    await fetch(`${environment.baseURL}event/${post.postId}`,{
      method : 'PATCH',
      body : JSON.stringify(post),
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : 'Bearer ' + this._authToken
      }
    })
  }

  async putInvitation(id : number, value : number) : Promise<void> {
    await fetch(`${environment.baseURL}invitation/${id}?answer=${value}`, {
      method : 'PUT',
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : 'Bearer ' + this._authToken
      }
    })
  }

  async putInviteToGroup(groupId : number, userId : number) : Promise<void> {
    await fetch(`${environment.baseURL}group/invite/${groupId}`, {
      method : 'PUT',
      body : JSON.stringify(userId),
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : 'Bearer ' + this._authToken
      }
    }).catch(error => {
      console.log(error);
    })
  }

  get allPosts() : Post[] | undefined {
    return this._posts;
  }
  private _post : Post | undefined
  get post ():Post | undefined{
    return this._post;
  }

  // setPostByID(id:number):void{
  //   this._post = POSTS[id];
  // }

 async addComment(comment:Reply): Promise<void>{
  await fetch(`${environment.baseURL}comment`,{
    method : 'POST',
    body : JSON.stringify(comment),
    headers : {
      'Content-Type' : 'application/json',
      'authorization' : 'Bearer ' + this.sessionService.authToken
    }
  })
  }
}

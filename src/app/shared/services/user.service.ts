import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { SocialUser } from 'angularx-social-login';
import { User } from 'src/app/profile/models/user';
import { environment } from 'src/environments/environment';
import { SessionService } from './session.service';
import { MatSnackBar } from '@angular/material/snack-bar';


const API_URL = 'https://experis-alumni-api.azurewebsites.net'
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl : string = environment.baseURL;

  private _socialUser : SocialUser | undefined

  constructor(
    private readonly http: HttpClient,
    private sessionService:SessionService,
    private _snackBar : MatSnackBar)
    {
    }
  async getUser(id : number) : Promise<User> {
    try {
      const response = await fetch(`${this.baseUrl}user/${id}`, {
        headers : {
          'Content-Type' : 'application/json',
          'Authorization' : 'Bearer ' + this.sessionService.authToken
        }
      });
      return await response.json()
    }  catch(error) {
      console.log(error);
      return {} as User;
    }
  }

   async getAuthenticatedUser() : Promise<User>{
    const response = await fetch(`${API_URL}/api/v1/user/getAuthorizedUser`, {
      method : 'GET',
      headers : {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      }
    })
    return await response.json();
  }

 async createUser(provider:string) : Promise<User>{
  await fetch(`${API_URL}/api/v1/user/createUser`, {
    method : 'POST',
    body : JSON.stringify(this.userForRegistration(provider)),
    headers : {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.sessionService.authToken
    }
  }).then(response => {
      return response.json();
  }).then((user : User) => {
    this.sessionService.currentUser = user;
    return user;
  })
  .catch(error => {
    console.log(error);
  });
  return {} as User
}

 async putUser(user : User){
    const body = JSON.stringify(user);
    await fetch(`${API_URL}/api/v1/user/${user.userId}`,
    {
      method: 'PATCH',
      body: body,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      }
    }).catch(error => {
      console.log(error);

    })
  }
  async getUserAll() : Promise<User[]> {
  	return await fetch(`${API_URL}/api/v1/user/getAll`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      }
    }).then((recivedList) => {
      return recivedList.json()
    })
    .catch((error) => { console.error('Error:', error);}
    );
  }

  async getListOfUsers(list:Set<number>){
    let users: Promise<User[]> | never[] = [];
    let URL:string = `${API_URL}/api/v1/user/getMultipleUsers?`
    for(var number of list){
      URL += `id=${number}&`
    }
    const response = await fetch(URL,{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method:'GET'
    });
    
    return await response.json();
  }
  //Brukes for registrering av bruker
 userForRegistration(provider:string){
    const user: any = {
      Name: this._socialUser?.name ?? '',
      ImgUrl: this._socialUser?.photoUrl ?? '',
      Email: this._socialUser?.email,
      Occupation:'',
      Bio: '',
      FunFact: '',
      Provider: provider,
    };
    return user;
  }
  setSocialUser(user: SocialUser): void {
    this._socialUser = user;
  }

  openSnackBarMessage(message : string, action : string) {
    this._snackBar.open(message, action, {
      duration : 3500,
      horizontalPosition : "center",
      verticalPosition : "bottom",
      panelClass : ['white-snackbar']
    });
  }
}

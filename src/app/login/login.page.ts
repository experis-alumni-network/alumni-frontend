import { Component, OnInit } from "@angular/core";

import { SocialAuthService, SocialUser } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";

import { Observable, Subscription } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from "src/app/shared/services/user.service";
import { User } from "src/app/profile/models/user";
import { SessionService } from "src/app/shared/services/session.service";

@Component({
    selector : 'app-login-page',
    templateUrl : './login.page.html',
    styleUrls : ['./login.page.css'],
})

export class LoginPage implements OnInit {
  public loggingIn : boolean = false;

    constructor(
        private readonly router: Router,
        private authService: SocialAuthService,
        private http: HttpClient,
        private userService: UserService,
        private sessionService:SessionService)
        {
}
    hasApiAcess:boolean = false;
    async signInWithGoogle() {
        this.loggingIn = true;
        await this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
        await this.ifLoggedIn();
      }


      signOut(): void {
        this.authService.signOut();
      }

     async ifLoggedIn(){
        let sub = this.authService.authState.subscribe((user: SocialUser) => {
            if (user) {
              this.http.post<any>('https://experis-alumni-api.azurewebsites.net/api/v1/user/authenticate', { idToken: user.idToken })
              .subscribe(async (authToken: any) => {
              this.sessionService.authToken = authToken.authToken;

              localStorage.setItem('authToken',authToken.authToken);
              this.userService.setSocialUser(user);
              await this.userService.createUser('Google');
              if(authToken){
                this.sessionService.isLoggedIn = true;

                this.router.navigate(['/']);
                this.userService.openSnackBarMessage("You are logged in!","Got it!");
              }else {
              }
               })
            }
          });
          sub.unsubscribe()
    }

    async checkIfAuthenticated(){
      const token = localStorage.getItem('authToken');
      if(token){
        try {
          this.sessionService.authToken = token;

          const data = await this.userService.getAuthenticatedUser();
          if(data){
              this.sessionService.currentUser = data;
              this.sessionService.isLoggedIn = true;
              this.router.navigate(['/']);
          }
        } catch (error) {
          console.log(error)
        }
      }
    }

    ngOnInit() {
      this.checkIfAuthenticated();
    }

    onSubmit(loginForm : NgForm) : void {
        const { username } = loginForm.value;
        this.router.navigate(['/']);
        this.userService.openSnackBarMessage("You are logged in!","Got it!");
    }
}



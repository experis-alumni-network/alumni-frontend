import { Reply } from "./reply";
import { User } from "../../profile/models/user";
import { Event } from "src/app/post/models/event";
import { Group } from "src/app/group/models/group";

export interface Post{
    postId : number,
    title : string,
    content : string,
    lastUpdated : Date,
    creationDate : Date,
    comments : Reply[],
    author : number,
    type : number,
    event? : Event,
    group? : number,
    topics : number[] | undefined,
}
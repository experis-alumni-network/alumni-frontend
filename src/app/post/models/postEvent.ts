import { Reply } from "./reply";

export interface PostEvent {
    postEventId : number,
    allowedGuests : number,
    startTime : Date,
    endTime : Date,
    bannerImg : string,
    isPrivate : boolean,
    //Relatedpost is the primary key of the post
    relatedPost : number,
    title : string,
    content : string,
    //Author is the primary key of the user who made the event
    author : number,
    type : number,
    group : number,
    comments : Reply[],
    subscribedUsers : number[],
    topics : number[] | undefined,

}
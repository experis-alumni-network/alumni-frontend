export interface Invitation {
    invitationId : number,
    answer : string,
    event : number,
    guest : number,
}
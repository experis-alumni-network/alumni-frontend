import { Post } from "src/app/post/models/post"

export interface Event{
    allowed_guests: number,
    bannerUrl: string,
    startTime: Date,
    endTime: Date,
    isPrivate : boolean,
    location : string,
}
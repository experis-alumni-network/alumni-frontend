import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-thread',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.css']
})
export class PostPage implements OnInit {
  constructor(
    private readonly router : Router, 
    private route: ActivatedRoute,)
    { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { USERS } from 'src/app/mock-users';
import { Post } from 'src/app/post/models/post';
import { PostEvent } from 'src/app/post/models/postEvent'
import { Reply } from 'src/app/post/models/reply';
import { User } from 'src/app/profile/models/user';
import { PostService } from 'src/app/shared/services/post.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { UserService } from 'src/app/shared/services/user.service';


@Component({
  selector: 'app-thread',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  private id : number = 1;
  constructor(
    private readonly  postService:PostService, 
    private route: ActivatedRoute, 
    private readonly sessionService : SessionService,
    private readonly userService : UserService,
    private readonly router : Router) { }
    private routeSub: Subscription | undefined;
    public postEvent : PostEvent | undefined = undefined
    public user : User = {} as User;
    public post : Post  = {} as Post;
    public edit : boolean = false;
    public showEvent : boolean = false;
    public showPost : boolean = false;
  async ngOnInit(): Promise<void> {
    this.routeSub = this.route.params.subscribe(async (params: Params): Promise<void> => {
      this.id = params['id'];

      this.post = await this.postService.getPost(this.id);
      this.user = await this.userService.getUser(this.post.author);
      
      if(this.post.author === this.sessionService.currentUser.userId) {
        this.edit = true;
      }
    });
    this.post = await this.postService.getPost(this.id);
    if(this.post.type === 1) {
      this.postEvent = await this.postService.getEvent(this.post.postId);
      this.showEvent = true;
    } else { this.showPost = true; }
    if(this.post.author === this.sessionService.currentUser.userId) {
      this.edit = true;
    }
  }

 async ngOnDestroy(): Promise<void> {
    this.routeSub?.unsubscribe();
}
}

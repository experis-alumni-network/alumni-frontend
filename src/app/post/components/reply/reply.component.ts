import { Component, Input, OnInit } from '@angular/core';
import { Reply } from 'src/app/post/models/reply';
import { User } from 'src/app/profile/models/user';
import { UserService } from 'src/app/shared/services/user.service';
import { comment } from '../../models/comment';

@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.css']
})
export class ReplyComponent implements OnInit {

  constructor(private readonly userService : UserService) { }

  @Input() reply: comment = {} as comment;

  async ngOnInit(): Promise<void> {
    
    
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { USERS } from 'src/app/mock-users';
import { User } from 'src/app/profile/models/user';
import { PostService } from 'src/app/shared/services/post.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { UserService } from 'src/app/shared/services/user.service';
import { Post } from '../../models/post';
import { Reply } from '../../models/reply';
import { comment } from '../../models/comment';
import { Group } from 'src/app/group/models/group';

@Component({
  selector: 'app-post-thread',
  templateUrl: './post-thread.component.html',
  styleUrls: ['./post-thread.component.css']
})
export class PostThreadComponent implements OnInit {
  @Input() post : Post = {} as Post
  public comments: comment[] = []
  public commentAuthors:User[] = [];
  public user : User = {} as User;
  public group : Group | undefined;
  public edit : boolean = false;

  constructor(
  private readonly postService : PostService, 
  private router : Router, 
  private readonly userService : UserService,
  private readonly sessionService : SessionService) { }

  async ngOnInit(): Promise<void> {
    this.user = await this.userService.getUser(this.post.author);

    const commentAuthorsId = new Set(this.post.comments.map(comment => comment.author));
    console.log(this.post);
    
    this.commentAuthors = await this.userService.getListOfUsers(commentAuthorsId);
    
    this.comments = this.post.comments.map(comment => this.matchAuthorToComment(comment));
    if(this.sessionService.currentUser.userId === this.post.author) { this.edit = true; }
    
  }
  //Uses this to get author object and not just id in comment object
  matchAuthorToComment(prevReply:Reply):comment{
    const reply: comment = {
      author: this.commentAuthors.find(user => user.userId == prevReply.author)!,
      content:prevReply.content
    } 
    console.log(reply);
    return reply;
  }

  async onSubmit(form : NgForm) : Promise<void> {
    const { comment } = form.value;
    if(comment == ''){
      return;
    }
    const reply: any = {
      post: this.post.postId,
      content:comment
    } 

    //This is so it updates in real time
    const comm: comment = {
      author:this.sessionService.currentUser,
      content:comment
    } 
    this.comments.push(comm);
    
    await this.postService.addComment(reply);
    form.reset();
    
  }

  goToEdit(id : number) {
    this.router.navigate([`/post/${id}/edit`])
  }

  onBackClick(){
    this.router.navigate([""])
  }
  
  handleClickOnGroup(groupId : number){
    this.router.navigate(["group", groupId])
  }

  handleClickOnUser(userId : number){
    this.router.navigate(["profile", userId])
  }
}

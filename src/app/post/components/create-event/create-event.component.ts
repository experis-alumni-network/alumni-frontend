import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Event } from 'src/app/post/models/event';
import { PostService } from 'src/app/shared/services/post.service';
import { Post } from '../../models/post';
import { PostComponent } from '../post/post.component';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit{

  @Input() post : Post = {} as Post;
  @Output() event = new EventEmitter<Event>();
  public allowed_guest : number = 0;
  public startDate : Date = new Date();
  public endDate : Date = new Date();
  public startTime : string = '0';
  public endTime : string = '0';
  public postEvent : Event = {} as Event;
  bannerUrl : string = 'https://www.bannerbatterien.com/upload/filecache/Banner-Batterien-Windrder2-web_06b2d8d686e91925353ddf153da5d939.webp';
  constructor(private readonly postService : PostService) { }

  async ngOnInit() {
    if(this.post.type === 1) {
      this.postEvent = await this.postService.getShortEvent(this.post.postId);
      this.allowed_guest = this.postEvent.allowed_guests;
    }
  }
  onChange() {
     const newEvent : Event = {
       startTime : this.formatStartDate(),
       endTime : this.formatEndDate(),
       allowed_guests : this.allowed_guest,
       bannerUrl : this.bannerUrl,
       isPrivate : false,
       location : "Oslo"
     }
    this.event.emit(newEvent);
  }
  onChangeImage(image : any) {
    this.bannerUrl = image.url;
  }

  formatStartDate() : Date {
    let arr = this.startDate.toString().split("-");
    let year = parseInt(arr[0]);
    let month = parseInt(arr[1]) -1;
    let day = parseInt(arr[2]);

    let hours = parseInt(this.startTime.slice(0,2));
    let minutes = parseInt(this.startTime.slice(-2));
    
    return new Date(year,month,day,hours,minutes);
  }

  formatEndDate() : Date {
    let arr = this.endDate.toString().split("-");
    let year = parseInt(arr[0]);
    let month = parseInt(arr[1]) -1;
    let day = parseInt(arr[2]);

    let hours = parseInt(this.endTime.slice(0,2));
    let minutes = parseInt(this.endTime.slice(-2));
    
    return new Date(year,month,day,hours,minutes);
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/profile/models/user';
import { InviteDialogComponent } from 'src/app/shared/components/invite-dialog/invite-dialog.component';
import { PostService } from 'src/app/shared/services/post.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { UserService } from 'src/app/shared/services/user.service';
import { Invitation } from '../../models/invitation';
import { PostEvent } from '../../models/postEvent';
import { Post } from '../../models/post';
import { Reply } from '../../models/reply';
import { comment } from '../../models/comment';
import { NgForm } from '@angular/forms';
import { Group } from 'src/app/group/models/group';
import { GroupService } from 'src/app/group/services/group.service';


@Component({
  selector: 'app-event-thread',
  templateUrl: './event-thread.component.html',
  styleUrls: ['./event-thread.component.css']
})
export class EventThreadComponent implements OnInit {
  @Input() event : PostEvent = {} as PostEvent;
  public comments: comment[] = []
  public post : Post = {} as Post;
  public group : Group | undefined;
  public user : User = {} as User;

  public invitation : Invitation = {} as Invitation;
  public invitedUsers : User[] = [];
  private id : number = 0;
  public showOptions : boolean = false;
  public userAnswers : any[] = [];
  public commentAuthors:User[] = [];
  private currentUserInvitation : Invitation = {} as Invitation;

  constructor(private readonly postService : PostService,
    private readonly route : ActivatedRoute,
    private readonly sessionService : SessionService,
    public dialog: MatDialog,
    private readonly userService : UserService,
    private readonly router : Router,
    private readonly groupService : GroupService,
  ) { }

  async ngOnInit(): Promise<void> {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
    await this.getUserAndInvitation();
    const commentAuthorsId = new Set(this.event.comments.map(comment => comment.author));
    
    this.commentAuthors = await this.userService.getListOfUsers(commentAuthorsId);
    
    this.comments = this.event.comments.map(comment => this.matchAuthorToComment(comment));

    this.post = await this.postService.getPost(this.event.relatedPost)
    if(this.event.group == 0){
      this.group = await this.groupService.getGroup(this.event.group)
    }
    this.user = await this.userService.getUser(this.event.author)
  }

  get userId() {
    return this.sessionService.currentUser.userId;
  }

  async getUserAndInvitation() {
    this.invitedUsers = await this.postService.getInvitedUsers(this.event.postEventId);
    this.invitedUsers.forEach(async (user : User) => {
      this.invitation = await this.postService.getsingleInvitationForUser(this.event.postEventId,user.userId);
      let obj = {
        "name" : user.name,
        "answer" : this.invitation.answer
      }
      if(user.userId === this.userId) {
        if(this.invitation.answer === "UNANSWERED") {
           this.showOptions = true;
           this.currentUserInvitation = this.invitation;
        }
      } else {
        this.userAnswers.push(obj);
      }
    })
  }
  async answerInvitation(value : number) {
    await this.postService.putInvitation(this.currentUserInvitation.invitationId,value);
    await this.getUserAndInvitation();
    let answer = "";
    switch(value){
      case 1:
        answer = "Yes";
        break;
      case 2:
        answer = "No";
        break;
      case 3:
        answer = "Maybe";
        break;
    }
    this.showOptions = false;
    this.userService.openSnackBarMessage(`You answered ${answer} to the invitation`, "Perfect")
  }

  async promptInvite(): Promise<void>{
    //create and open dialog, subscribe to chosenNames, and invite users when we get chosenNames
    const dialogConfig = new MatDialogConfig();
    let dialogInstance = this.dialog.open(InviteDialogComponent, dialogConfig)
    dialogInstance.afterClosed()
      .subscribe(chosenNames => this.inviteUsers(allUsers.filter(user => chosenNames.includes(user.name))));

    //get users and send them to dialog
    let allUsers = await this.userService.getUserAll();
    //Remove yourself from the invite array
    const index = allUsers.findIndex(u => u.userId === this.userId)
    if(index > -1) {
      allUsers.splice(index,1);
    }
    let allNames = allUsers.map(user => user.name)
    dialogInstance.componentInstance.allNames = allNames;
  }

  inviteUsers(users : User[]) : void{
    //TODO: Handle http call to send invites
    users.forEach(user => {
      this.postService.postInviteUserToEvent(user.userId, this.event.postEventId).catch(error =>{
        this.userService.openSnackBarMessage("Could not invite user", "Okay :/")
      })
    })
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

  matchAuthorToComment(prevReply:Reply):comment{
    const reply: comment = {
      author: this.commentAuthors.find(user => user.userId == prevReply.author)!,
      content:prevReply.content
    }
    return reply;
  }

  async onSubmit(loginForm : NgForm) : Promise<void> {
    const { comment } = loginForm.value;
    const reply: any = {
      post: this.event.relatedPost,
      content:comment
    }

    //This is so it updates in real time
    const comm: comment = {
      author:this.sessionService.currentUser,
      content:comment
    }
    this.comments.push(comm);

    await this.postService.addComment(reply);
    loginForm.reset();
  }

  onBackClick(){
    this.router.navigate([""])
  }

  handleClickOnGroup(groupId : number){
    this.router.navigate(["group", groupId])
  }

  handleClickOnUser(userId : number){
    this.router.navigate(["profile", userId])
  }
}

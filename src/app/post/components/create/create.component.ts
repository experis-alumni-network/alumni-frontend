import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Post } from 'src/app/post/models/post';
import { Event } from 'src/app/post/models/event';
import snarkdown from 'snarkdown';
import { PostService } from 'src/app/shared/services/post.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/profile/models/user';
import { SessionService } from 'src/app/shared/services/session.service';
import { USERS } from 'src/app/mock-users';


@Component({
  selector: 'app-thread-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public previewOpen : boolean = false;
  public infoOpen : boolean = false;
  public showEvent : boolean = false;
  public edit : boolean = false;
  public post : Post = {} as Post;
  public titleValue : string = "";
  public contentValue : string = "";
  public eventFromChild : Event = {} as Event
  private us : User = {} as User;
  public previewPost : Post = {} as Post;
  constructor(
    private readonly postService : PostService, 
    private readonly userService : UserService,
    private readonly router : Router) { }

  async ngOnInit(): Promise<void> {

  }
    async handleSubmitFromChild(post : any) {
      if(post.type === 0) {
        await this.postService.postPost(post).then(() => {
          this.userService.openSnackBarMessage("Post successfully posted", "Great!");
        })
      } else {
        await this.postService.postEvent(post).then(() => {
          this.userService.openSnackBarMessage("Event successfully created","Superb");
        })
      }
      this.router.navigate(['/'])
    }
}

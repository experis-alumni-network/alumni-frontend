import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/shared/services/post.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { Post } from '../../models/post';
import { Event } from '../../models/event';
import { PostEvent } from '../../models/postEvent';
import { Topic } from 'src/app/topic/models/topic';
import { TopicService } from 'src/app/topic/services/topic.service';
import { UserService } from 'src/app/shared/services/user.service';
import { Group } from 'src/app/group/models/group';
import { GroupService } from 'src/app/group/services/group.service';
import { User } from 'src/app/profile/models/user';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  @Input() post : Post = {} as Post;
  @Output() outPost : EventEmitter<any> = new EventEmitter<any>();
  public eventFromChild : Event = {} as Event
  public previewPost : Post = {} as Post;
  private id : number = 0;
  public infoOpen : boolean = false;
  public showEvent : boolean = false;
  public previewOpen : boolean = false;
  public titleValue :string = "";
  public contentValue : string = "";
  public privacy : boolean = false;
  public edit : boolean = false;
  public topics : Topic[] = [];
  public groups : Group[] = [];
  constructor(private readonly route : ActivatedRoute,
     private readonly postService : PostService,
     private readonly sessionService : SessionService,
     private readonly topicService : TopicService,
     private readonly groupService : GroupService,
     private readonly userservice: UserService) { }

  async ngOnInit(): Promise<void> {
    this.edit = Object.keys(this.post).length !== 0;
    if(this.edit) {
      this.titleValue = this.post.title;
      this.contentValue = this.post.content;
    }
    this.topics = await this.topicService.getTopics();
    this.groups = await this.groupService.getGroups();
  }

  get user() {
    return this.sessionService.currentUser
  }

  handleInfoClick() {
    this.infoOpen = !this.infoOpen;
  }

  onSubmit(form : NgForm) {
    if(!form.valid){
      this.userservice.openSnackBarMessage("Fill out the required fields","Got it!")
      return;
    }
    //Make postEvent
    if(this.showEvent) {
      if(this.checkIfEventIsValid()){
        this.userservice.openSnackBarMessage("Fill out the required fields","Got it!");
        return;
      }
      const postEvent = {} as PostEvent;
      postEvent.title = form.value.title;
      postEvent.content = form.value.content;
      postEvent.allowedGuests = this.eventFromChild.allowed_guests;
      postEvent.bannerImg = this.eventFromChild.bannerUrl || "";
      postEvent.startTime = this.eventFromChild.startTime;
      postEvent.endTime = this.eventFromChild.endTime
      postEvent.type = 1;
      postEvent.group = form.value.group.groupId;
      if(form.value.topic.topicId == undefined){
        postEvent.topics = undefined
      }
      else{
        postEvent.topics = [form.value.topic.topicId]
      }
      if(form.value.isPrivate === "Private") {
        postEvent.isPrivate = true;
      } else { postEvent.isPrivate = false; }
      this.userservice.openSnackBarMessage("Posting...","");
      this.outPost.emit(postEvent);
      return;
    } else {
      //Make post
      const post = {} as Post;
    post.title = form.value.title;
    post.content = form.value.content;
    post.type = 0; 
    if(form.value.topic.topicId == undefined){
      post.topics = undefined
    }
    else{
      post.topics = [form.value.topic.topicId]
    }
    post.group = form.value.group.groupId;
    post.event = undefined;
    post.author = this.sessionService.currentUser.userId;
    
    this.userservice.openSnackBarMessage("Posting...","");
    this.outPost.emit(post);
    return;
    }
  }

  onChangeEvent(form : NgForm) {
    this.showEvent = !this.showEvent;
    // this.updateEvent(form);
  }

  checkIfEventIsValid(){
    if(this.eventFromChild.endTime){
      return false
    }
    if(this.eventFromChild.startTime){
      return false
    }
    if(this.eventFromChild.allowed_guests){
      return false
    }
    return true;
  }

  handleChangeFromChild(event : any) {
    this.eventFromChild = event as Event;

  }

  handlePreviewClick(form : NgForm) {
    this.previewPost = this.updatePost(form);
    this.previewOpen = true;
  }

  closePreview() {
    this.previewOpen = false;
  }

  updatePost(postForm : NgForm) {
    const post = {} as Post;
    post.title = postForm.value.title;
    post.content = postForm.value.content;
    post.creationDate = new Date();
    post.lastUpdated = this.post.creationDate;
    post.comments = [];
    if(!this.showEvent){
      post.type = 0;
    }
    else{
      post.type = 1;
      post.event = this.eventFromChild;
    }
    
    return post
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/shared/services/post.service';
import { UserService } from 'src/app/shared/services/user.service';
import { Post } from '../../models/post';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

  public post : Post | undefined = undefined;
  private id : number = 0;
  constructor(private readonly postService : PostService,
    private readonly route : ActivatedRoute,
    private readonly userService : UserService,) { }

  async ngOnInit(): Promise<void> {
    this.route.params.subscribe(param => {
      this.id = param['id'];
    })
    this.post = await this.postService.getPost(this.id);
  }

  async handleSubmitFromChild(post : Post) {
    if(post.event !== undefined) {
      await this.postService.patchPost(post).then(() => {
        this.userService.openSnackBarMessage("Post edited","Thanks!");
      });
    } else {
      await this.postService.patchEvent(post).then(() => {
        this.userService.openSnackBarMessage("Event edited","Perfect!");
      });
    }
    
  }

}

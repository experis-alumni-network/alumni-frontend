import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { SocialLoginModule, SocialAuthServiceConfig} from "angularx-social-login";
import { GoogleLoginProvider} from "angularx-social-login";

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalenderComponent } from './shared/components/calender/calender.component';
import { DetailComponent } from './shared/components/detail/detail.component';
import { PostThreadComponent } from './post/components/post-thread/post-thread.component';
import { CreateComponent } from './post/components/create/create.component';
import { HomePage } from './home/pages/home.page';
import { ProfilePage } from './profile/pages/profile.page';
import { PostPage } from './post/pages/post.page';
import { GroupPage } from './group/pages/group/group.page';
import { LoginPage } from './login/login.page';
import { TopicPage } from './topic/pages/topic/topic.page';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { EditProfileComponent } from './profile/components/edit-profile/edit-profile.component';
import { FileUploadComponent } from './shared/components/file-upload/file-upload.component';
import { ReplyComponent } from './post/components/reply/reply.component';
import { PostComponent } from './post/components/post/post.component';
import { ErrorComponent } from './shared/components/error/error/error.component';
import { ProfileComponent } from './profile/components/profile/profile.component';
import { FeedItemComponent } from './shared/components/feed-item/feed-item.component';
import { FeedComponent } from './shared/components/feed/feed.component';
import { DashboardComponent } from './shared/components/dashboard/dashboard.component';
import { CalendarEventComponent } from './shared/components/calendar-event/calendar-event.component';
import { CreateGroupComponent } from './group/components/create-group/create-group.component';
import { GroupComponent } from './group/components/group/group.component';
import { TopicComponent } from './topic/components/topic/topic.component';
import { InviteDialogComponent } from './shared/components/invite-dialog/invite-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MAT_FORM_FIELD, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatChipList, MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon'
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CreateEventComponent } from './post/components/create-event/create-event.component';
import { EditPostComponent } from './post/components/edit-post/edit-post.component';
import { TopicListComponent } from './topic/components/topic-list/topic-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { PostFormComponent } from './post/components/post-form/post-form.component';
import { MatCardModule } from '@angular/material/card';
import { TimeagoModule } from 'ngx-timeago';
import { TimeagoIntl, TimeagoFormatter, TimeagoCustomFormatter } from 'ngx-timeago';
import { GroupListComponent } from './group/components/group-list/group-list.component';
import { GroupListItemComponent } from './group/components/group-list-item/group-list-item.component';
import { EventThreadComponent } from './post/components/event-thread/event-thread.component';
import { TopicItemComponent } from './topic/components/topic-item/topic-item.component';
import { TopicCreateComponent } from './topic/components/topic-create/topic-create.component';
import { ImageUrlDialogComponent } from './shared/components/image-url-dialog/image-url-dialog.component';

export class MyIntl extends TimeagoIntl {
  // do extra stuff here...
}
@NgModule({
  declarations: [
    AppComponent,
    CalenderComponent,
    DetailComponent,
    // PostListComponent,
    // ListComponent,
    PostThreadComponent,
    CreateComponent,
    HomePage,
    ProfilePage,
    PostPage,
    GroupPage,
    LoginPage,
    NavbarComponent,
    FileUploadComponent,
    TopicPage,
    ReplyComponent,
    PostComponent,
    ErrorComponent,
    EditProfileComponent,
    ProfileComponent,
    FeedItemComponent,
    FeedComponent,
    DashboardComponent,
    CalendarEventComponent,
    CreateGroupComponent,
    GroupComponent,
    TopicComponent,
    InviteDialogComponent,
    // MatChipList,
    CreateEventComponent,
    EditPostComponent,
    TopicListComponent,
    PostFormComponent,
    EventThreadComponent,
    TopicItemComponent,
    GroupListItemComponent,
    GroupListComponent,
    TopicCreateComponent,
    ImageUrlDialogComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    SocialLoginModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatChipsModule,
    MatIconModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    NoopAnimationsModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    TimeagoModule.forRoot({
      intl: { provide: TimeagoIntl, useClass: MyIntl },
      formatter: { provide: TimeagoFormatter, useClass: TimeagoCustomFormatter },
    }),
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'fill'}
    },
    {
      provide: MAT_FORM_FIELD, useValue:{}
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '834242937946-to54thu5pf9ssjm8p79eekotqv1nm71s.apps.googleusercontent.com'
              )
            }
          ],
          onError: (err) => {
            console.error(err);
          }
        } as SocialAuthServiceConfig,
      }
      
    ],
    bootstrap: [AppComponent],
    entryComponents: [
      InviteDialogComponent,
    ],
  })
export class AppModule { }


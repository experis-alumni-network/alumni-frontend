import { Post } from "./post/models/post";
import { USERS } from "./mock-users";
import { REPLIES, REPLIES2 } from "./mock-replies";
import { Event } from "./post/models/event";
import { Reply } from "./post/models/reply";

export const EVENTS : Event[] = [
    {
        allowed_guests : 40,
        bannerUrl : "https://media.istockphoto.com/photos/blurred-people-making-party-throwing-confetti-young-people-on-night-picture-id1071000086?k=20&m=1071000086&s=612x612&w=0&h=gcRA0L3VQMITNdGYkcanOcXXG5DtTRIhGp7ZRVOzbpc=",
        startTime : new Date(2021, 9, 16, 19, 0, 0),
        endTime : new Date(2021, 9, 16, 23, 59, 0),
        isPrivate : false,
        location : "Oslo",
    },
    {
        allowed_guests : 40,
        bannerUrl : "https://www.brickunderground.com/sites/default/files/styles/blog_primary_image/public/blog/images/Screen%20Shot%202015-06-10%20at%209.54.48%20PM.png",
        startTime : new Date(2021, 9, 16, 21, 0, 0),
        endTime : new Date(2021, 9, 16, 23, 0, 0),
        isPrivate : false,
        location : "Oslo",
    },
    {
        allowed_guests : 40,
        bannerUrl : "https://images.sunsationalswimschool.com/blog/the_ultimate_pool_party_a_sunsational_time_for_all/best-pool-party-ideas-sunsational.jpg",
        startTime : new Date(2021, 10, 2, 12, 0, 0),
        endTime : new Date(2021, 10, 2, 18, 0, 0),
        isPrivate : false,
        location : "Oslo",
    },

]
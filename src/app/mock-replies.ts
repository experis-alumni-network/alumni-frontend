import { Reply } from "./post/models/reply";
import { USERS } from './mock-users';

export const REPLIES: Reply [] = [
    {
        author: USERS[0].userId,
        content: "You suck harry!"
    },
    {
        author: USERS[0].userId,
        content: "You racist!"
    },
    {
        author: USERS[2].userId,
        content: "#HalfbloodShit. You are a racist bastard harry! To think that I once loved you is impossible to understand now!"
    },
    {
        author: USERS[2].userId,
        content: "Muggles are people too!"
    }
]

export const REPLIES2: Reply [] = [
    {
        author: USERS[0].userId,
        content: "Sisters before misters!"
    },
    {
        author: USERS[1].userId,
        content: "I still love you! It is unfortunate that you belong to an inferior people :("
    },
    {
        author: USERS[2].userId,
        content: "I cant take this!"
    }
]

export const REPLIES3 : Reply[] = [
    {
        author: USERS[1].userId,
        content: "All muggles should be sent to concentration camps! \n regards The new halfblood prince "
    },
    {
        author: USERS[2].userId,
        content: "I Never want to see him ever again! He said that I should die in a concentration camp!"
    },
]

export const REPLIES4 : Reply[] = [
    {
        author: USERS[2].userId,
        content: "I do not like this"
    },
    {
        author: USERS[0].userId,
        content: "fuck you mate, this is very good"
    },
    {
        author: USERS[2].userId,
        content: "i dont like you either"
    },
]

export const REPLIES5 : Reply[] = [
    {
        author: USERS[1].userId,
        content: "lets get this party started"
    },
    {
        author: USERS[0].userId,
        content: "woop woop"
    },
    {
        author: USERS[2].userId,
        content: "im gonna sing \"Trumpets\" by jason derulo all night"
    },
    {
        author: USERS[1].userId,
        content: ":cringe:"
    },
]

export const REPLIES6 : Reply[] = [
    {
        author: USERS[1].userId,
        content: "wont it be cold? fucking idiot..."
    },
]
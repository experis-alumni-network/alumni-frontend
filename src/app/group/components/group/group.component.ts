import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { InviteDialogComponent } from 'src/app/shared/components/invite-dialog/invite-dialog.component';
import { PostService } from 'src/app/shared/services/post.service';
import { Group } from '../../models/group';
import { GroupService } from '../../services/group.service';
import { MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { UserService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/profile/models/user';
// import {BrowserAnimationsModule} from '@angular/animations'
@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  private id : number = 0;
  group : Group = {} as Group

  constructor(
    private readonly route : ActivatedRoute,
    private readonly groupService : GroupService,
    private userService : UserService,
    public dialog: MatDialog
    ) { }

  ngOnInit(): void{
    this.route.params.subscribe(params => {this.id = +params.id})
    this.groupService.getGroup(this.id).then(g => this.group = g);
  }

  async promptInvite(): Promise<void>{

    //create and open dialog, subscribe to chosenNames, and invite users when we get chosenNames
    const dialogConfig = new MatDialogConfig();
    let dialogInstance = this.dialog.open(InviteDialogComponent, dialogConfig)
    dialogInstance.afterClosed()
      .subscribe(chosenNames => this.inviteUsers(allUsers.filter(user => chosenNames.includes(user.name))));

    //get users and send them to dialog
    let allUsers = await this.userService.getUserAll()
    let allNames = allUsers.map(user => user.name)
    dialogInstance.componentInstance.allNames = allNames;
  }

  inviteUsers(users : User[]) : void{
    this.groupService.inviteToGroup(this.group.groupId!, users)
  }

}

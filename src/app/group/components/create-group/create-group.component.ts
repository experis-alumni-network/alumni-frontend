import { ThisReceiver } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, RouteConfigLoadEnd, Router } from '@angular/router';
import { SessionService } from 'src/app/shared/services/session.service';
import { Group } from '../../models/group';
import { GroupService } from '../../services/group.service';
import {UserService} from "../../../shared/services/user.service";

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {

  public isPublic : string = "public"
  constructor(
    private route : ActivatedRoute,
    private router : Router,
    private sessionService : SessionService,
    private groupService : GroupService,
    private userService : UserService
  ) { }

  ngOnInit(): void {
  }

  async ngOnSubmit(postForm : NgForm): Promise<void>{
    let newGroup : Group =
    {
      name : postForm.value.groupName,
      description : postForm.value.groupDesc,
      isPrivate : (postForm.value.isPrivate === 'public') ? false : true,
    }
    console.log(newGroup);

    this.groupService.postGroup(newGroup).then((postedGroup : Group) => {
      this.userService.openSnackBarMessage("Group created", "Cool!");
      this.router.navigate(["group", postedGroup.groupId]);
    })
  }

  onBackClick(){
    this.router.navigate(["group"])
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Group } from 'src/app/group/models/group';
import { GroupService } from 'src/app/group/services/group.service';
import { TopicService } from 'src/app/topic/services/topic.service';
import { SessionService } from '../../../shared/services/session.service';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit {
  public groups : Group[] = [] as Group[];
  public currentUserId : number = 0;

  constructor(
    private readonly router : Router,
    private readonly groupService : GroupService,
    private readonly topicService : TopicService,
    private readonly sessionService : SessionService,) { }

  async ngOnInit(): Promise<void> {
    this.groups = await this.groupService.getGroups();
    this.currentUserId = this.sessionService.currentUser.userId;
    await this.updateArray();
  }

  goToCreateGroup(){
    this.router.navigate(['/group/create']);
  }

  async updateArray() : Promise<void> {
    return await new Promise(async resolve => {
      for(let i = 0; i < this.groups.length; i++) {
        if(this.groups[i].isPrivate) {
          if(!(this.groups[i].groupMembers?.includes(this.currentUserId))) {
            this.groups.splice(i,1)
          }
      }
    }
    resolve();
    })
  }
}

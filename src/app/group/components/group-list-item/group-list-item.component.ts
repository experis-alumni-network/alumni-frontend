import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/shared/services/session.service';
import { Group } from '../../models/group';
import { GroupService } from '../../services/group.service';
import {UserService} from "../../../shared/services/user.service";

@Component({
  selector: 'app-group-list-item',
  templateUrl: './group-list-item.component.html',
  styleUrls: ['./group-list-item.component.css']
})
export class GroupListItemComponent implements OnInit {
  @Input() group : Group = {} as Group;
  isMember : boolean = false;
  currentUserId : number = 0;

  constructor(
    private readonly router : Router,
    private readonly sessionService : SessionService,
    private readonly groupService : GroupService,
    private readonly userService : UserService
  ) { }

  ngOnInit(): void {
    this.currentUserId = this.sessionService.currentUser.userId;
    if (this.group.groupMembers?.includes(this.currentUserId)){
      this.isMember=true;
    }
  }

  async onJoinClick(groupId : number){
    const updatedGroup = await this.groupService.joinGroup(groupId, this.currentUserId);
    this.userService.openSnackBarMessage("You joined the group!","WOOOO")
    this.isMember=true;
  }

  onGroupClick(id : number){
    this.router.navigate([`/group/${id}`])
  }
}

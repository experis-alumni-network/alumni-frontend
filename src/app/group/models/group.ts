export interface Group {
    groupId? : number,
    name : string,
    description : string,
    isPrivate : boolean,
    groupMembers? : number[],
};
import { Injectable } from '@angular/core';
import { Topic } from 'src/app/topic/models/topic';
import { Group } from '../models/group';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { User } from 'src/app/profile/models/user';
import { Post } from 'src/app/post/models/post';
import { SessionService } from 'src/app/shared/services/session.service';

// const httpOptions = {
//   headers: new HttpHeaders({
//     "Content-Type":  'application/json',
//     Authorization: 'my-auth-token'
//   })
// };

const httpHeader = {
  headers:
  {
    "Content-Type":  'application/json',
    "Accept": 'application/json'
  }
};

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  public groups : Group[] = [];
  public error : string = "";
  constructor(
    private http: HttpClient,
    private readonly sessionService: SessionService,
  ) { }

  async getGroups() : Promise<Group[]> {
    try{
      const response = await fetch(`${environment.baseURL}group`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.sessionService.authToken
        },
        method:'GET'
      });
      return response.json();
    } catch(error) {
      console.log(error);
      return [];
    }
  }

  async getGroup(groupId : number) : Promise<Group> {
    const group = await fetch(environment.baseURL + 'group/' + groupId, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method: "GET",
    })
    return group.json()
  }

  async getGroupPosts(groupId : number) : Promise<Post[]> {
    return await fetch(`${environment.baseURL}group/${groupId}/posts`, {
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : `Bearer ${this.sessionService.authToken}`
      },
    }).then(response => {
      return response.json();
    }).catch(error => {
      console.log(error);
      return [];
    })
  }

  async postGroup(group : Group) : Promise<Group> {
    const postedGroup = await fetch(environment.baseURL + 'group/create', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method: "POST",
      body: JSON.stringify(group)
    })
    return postedGroup.json()
  }

  async inviteToGroup(groupId : number, users : User[]){
    await fetch(environment.baseURL + 'group/invite/' + groupId, {
      headers : {
        'Content-Type' : 'application/json',
        'authorization' : `Bearer ${this.sessionService.authToken}`
      },
      method: "PUT",
      body: JSON.stringify(users.map(user => user.userId))
    })
  }

  async joinGroup(groupId : number, userId : number) : Promise<Group>{
    const joinedGroup = await fetch(environment.baseURL + 'group/join/' + groupId, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sessionService.authToken
      },
      method: "PUT",
      body: JSON.stringify(userId)
    })
    return joinedGroup.json()
  }

  handleError(failedRequest: string, item: Group) : any{
    console.log("The request: \"", failedRequest, "\" failed for the following object: ")
  }
}


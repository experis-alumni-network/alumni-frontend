import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/shared/services/session.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.page.html',
  styleUrls: ['./group.page.css']
})
export class GroupPage implements OnInit {
  loggedIn: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sessionService: SessionService,
  ) { }

  ngOnInit(): void {
    // this.sessionService.fetchUser() //development only, not nessecary, login should do this.

    this.loggedIn = this.sessionService.currentUser != undefined
  }

  goToLogin(): void{
    this.router.navigate(["login"]);
  }
}

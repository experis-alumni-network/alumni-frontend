import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './post/components/create/create.component';
import { EditProfileComponent } from './profile/components/edit-profile/edit-profile.component';
import { PostComponent } from './post/components/post/post.component';
import { GroupPage } from './group/pages/group/group.page';
import { HomePage } from './home/pages/home.page';
import { LoginPage } from './login/login.page';
import { ProfilePage } from './profile/pages/profile.page';
import { PostPage } from './post/pages/post.page';
import { TopicPage } from './topic/pages/topic/topic.page';
import { ErrorComponent } from './shared/components/error/error/error.component';
import { ProfileComponent } from './profile/components/profile/profile.component';
import { GroupListComponent } from './group/components/group-list/group-list.component';
import { CalenderComponent } from './shared/components/calender/calender.component';
import { CreateGroupComponent } from './group/components/create-group/create-group.component';
import { DashboardComponent } from './shared/components/dashboard/dashboard.component';
import { AuthGuardService } from './shared/services/auth-guard.service';
import { GroupComponent } from './group/components/group/group.component';
import { TopicComponent } from './topic/components/topic/topic.component';
import { TopicListComponent } from './topic/components/topic-list/topic-list.component';
import { EditPostComponent } from './post/components/edit-post/edit-post.component';
import { TopicCreateComponent } from './topic/components/topic-create/topic-create.component';

const routes: Routes = [
  {
    path : '',
    component : HomePage,
    canActivate : [AuthGuardService]
  },
  {
    path : 'login',
    component : LoginPage,
  },
  {
    path : 'profile',
    component : ProfilePage,
    canActivateChild : [AuthGuardService],
    children: [
      {
        path : ':id',
        component : ProfileComponent,
      },
      {
        path : ':id/edit',
        component : EditProfileComponent,
      },
      {
        path : ':id/dashboard',
        component : DashboardComponent,
      },
      {
        path : '',
        pathMatch : 'full',
        component : ProfilePage
      }
    ],
  },
  {
    path : 'topic',
    component : TopicPage,
    canActivateChild : [AuthGuardService],
    children : [
      {
        path : 'create',
        component : TopicCreateComponent
      },
      {
        path : ':id',
        component : TopicComponent
      },
      {
        path : ':id/dashboard',
        component : DashboardComponent
      },
      {
        path : '',
        pathMatch : 'full',
        component : TopicListComponent
      }
    ]
  },
  {
    path : 'group',
    component : GroupPage,
    canActivateChild : [AuthGuardService],
    children : [
      {
        path : 'create',
        component : CreateGroupComponent,
      },
      {
        path : ':id',
        component : GroupComponent,
      },
      {
        path : '',
        pathMatch : 'full',
        component : GroupListComponent
      }
    ]
  },
  {
    path : 'post',
    component : PostPage,
    canActivateChild : [AuthGuardService],
    children : [
      {
        path : 'create',
        component : CreateComponent
      },
      {
        path : ':id/edit',
        component : EditPostComponent
      },
      {
        path : ':id',
        component : PostComponent,
      },
     
      {
        path : '',
        pathMatch : 'full',
        redirectTo : '/thread/create'
      }
    ]
  },
  {
    path : '**',
    pathMatch : 'full',
    component : ErrorComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

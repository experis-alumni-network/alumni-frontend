import { User } from "./profile/models/user";

export const USERS: User[] = [
    {
        userId : 1,
        name : "Henrik Eijsink",
        imgURL : "https://g.acdn.no/obscura/API/dynamic/r1/ece5/tr_1000_2000_s_f/0000/osbl/2016/9/26/11/1474881616150.jpg?chk=B9ADD5",
        bio : "This guy learning git is literally equivalent to a hippo trying to learn how to fly",
        funFact : "Tall boi",
        occupation: "",
        provider: "",
        email:"",
        subscribedPosts: [],
        topics: [],
        authoredComments:[],
        authoredPosts:[]
    },
    {
        userId : 2,
        name : "Harry Potter",
        imgURL : "https://www.temashop.no/media/catalog/product/cache/cat_resized/1200/0/h/a/harry_potter_briller_tilbeh_r_briller.jpg",
        bio : "Defeats dark lords for a living",
        funFact : "Scar in my face was actually just me tripping and faceplanting in 3rd grade",
        occupation: "",
        provider: "",
        email:"",
        subscribedPosts: [],
        topics: [],
        authoredComments:[],
        authoredPosts:[]
    },
    {
        userId: 3,
        name : "Hermoine GRanger",
        imgURL : "https://no.erch2014.com/images/iskusstvo-i-razvlecheniya/kakoe-zhe-ee-nastoyashee-imya-germiona-grejndzher.jpg",
        bio : "Books. I read them all",
        funFact : "I was always in love with Harry",
        occupation: "",
        provider: "",
        email:"",
        subscribedPosts: [],
        topics: [],
        authoredComments:[],
        authoredPosts:[]
    },
    {
        userId : 4,
        name : "Martin",
        imgURL : "https://media-exp1.licdn.com/dms/image/C4E03AQGurbADM3gaxg/profile-displayphoto-shrink_200_200/0/1628964138573?e=1637798400&v=beta&t=WF2LZvSj0El5jfL91liOFzMYOkrcMpsrZcF76aUym6w",
        bio : "asd",
        funFact : "Big developer",
        occupation: "",
        provider: "",
        email:"",
        subscribedPosts: [],
        topics: [],
        authoredComments:[],
        authoredPosts:[]
    },
]